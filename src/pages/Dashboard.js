import { useState, useEffect } from 'react';
import { Button, Table, Modal, Form } from 'react-bootstrap';
import { useNavigate  } from 'react-router-dom';
import Swal from 'sweetalert2';

import '../components/myStyles.css'
import 'animate.css'

export default function Dashboard() {


	const navigate = useNavigate()

	function createProduct() {   
	    
	    fetch(`${process.env.REACT_APP_API_URL}/products/checkProduct`, {
	        method: "POST",
	        headers: {
	            'Content-Type': 'application/json'
	        },
	        body: JSON.stringify({
	            name: newProduct.name,
	            
	        })
	    })
	    .then(res => res.json())
	    .then(data => {
	        console.log(data)

	        if (data.exist === true) {

	            Swal.fire({
	                title: "Duplicate Product Found",
	                icon: "error",
	                text: "Kindly provide another product.",
	                showClass: {
	                    popup: 'animate__animated animate__fadeInDown'
	                  },
	                  hideClass: {
	                    popup: 'animate__animated animate__fadeOutUp'
	                  }
	            })
	        } else {

	            fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
	                method: "POST",
	                headers: {
	                    'Content-Type': 'application/json'
	                },
	                body: JSON.stringify({
	                    name: newProduct.name,
	                    description: newProduct.description,
	                    price: newProduct.price
	                })
	            })
	            .then(res => res.json())
	            .then(data => {
	                console.log(data)

	                if(data === true) {
	                
	                // setFirstName("");
	                // setLastName("")
	                // setName("");
	                // setDescription("");
	                // setPrice("");

	                Swal.fire({
	                        title: "Product Created",
	                        icon: "success",
	                        text: "Nice!",
	                        showClass: {
	                            popup: 'animate__animated animate__fadeInDown'
	                          },
	                          hideClass: {
	                            popup: 'animate__animated animate__fadeOutUp'
	                          }
	                })
	                .then(()=>{
		                   window.location.reload();
		              })


	                //reloads page after selecting button from swal
		                
	                    navigate("/dashboard");


	                } else {

	                    Swal.fire({
	                        title: "Something went wrong",
	                        icon: "error",
	                        text: "Please, try again.",
	                        showClass: {
	                            popup: 'animate__animated animate__fadeInDown'
	                          },
	                          hideClass: {
	                            popup: 'animate__animated animate__fadeOutUp'
	                          }
	                    })
	                }
	            })
	        }
	    })

	    
	}


//-------------------------------------------------------------



	const [newProduct, setNewProduct] = useState({
	  name: '',
	  description: '',
	  price: '',
	  isActive: false,
	});

	const handleSave = () => {
	  setNewProduct({ name: '', description: '', price: '', isActive: false });
	  createProduct()
	};

	const handleSubmit = (e) => {
	  e.preventDefault();
	  handleSave();
	  handleClose();
	};
 
//-------------------------------------------------------------------------------
	// Modal useState

  const [show, setShow] = useState(false);

  const handleClose = () => {
  	setShow(false);
  	setNewProduct({
      name: "",
      description: "",
      price: ""
    });
  }
  const handleShow1 = () => setShow(true);


//-------------------------------------------------------------------------------

//Activate/Deactivate Product

	const handleActivate = (id) => {
		Swal.fire({
		  title: 'Are you sure?',
		  text: 'Do you want to activate this item?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Yes',
		  cancelButtonText: 'No',
		  showClass: {
		      popup: 'animate__animated animate__fadeInDown'
		    },
		    hideClass: {
		      popup: 'animate__animated animate__fadeOutUp'
		    }
		})
		.then((result) => {
    if (result.value) {
      // function to activate item by id
      activateProduct(id);

      Swal.fire({
        title: 'Congratulations!',
        text: 'You have successfully activated the product!',
        icon: 'succes',
        
        confirmButtonText: 'OK',
        
        showClass: {
            popup: 'animate__animated animate__fadeInDown'
          },
          hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
          }
      }).then(()=>{window.location.reload();})
    } else {
      // do nothing, just close the alert
    }
		})
	};
		


	function activateProduct(id) {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/activate`, {
			method: 'PATCH',
			
		})
	}


const handleDeactivate = (id) => {
		Swal.fire({
		  title: 'Are you sure?',
		  text: 'Do you want to deactivate this item?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Yes',
		  cancelButtonText: 'No',
		  showClass: {
		      popup: 'animate__animated animate__fadeInDown'
		    },
		    hideClass: {
		      popup: 'animate__animated animate__fadeOutUp'
		    }
		})
		.then((result) => {
    if (result.value) {
      // function to activate item by id
      deactivateProduct(id);

      Swal.fire({
        title: 'Congratulations!',
        text: 'You have successfully deactivated the product!',
        icon: 'succes',
        
        confirmButtonText: 'OK',
        
        showClass: {
            popup: 'animate__animated animate__fadeInDown'
          },
          hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
          }
      }).then(()=>{window.location.reload();})
    } else {
      // do nothing, just close the alert
    }
		})

	};
		


	function deactivateProduct(id) {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
			method: 'PATCH',
			
		})
	}

//-------------------------------------------------------------------------------
		const [productData, setProductData] = useState([])

		const [updateProduct, setUpdateProduct] = useState({
			name: '',
			description: '',
			price: ''
		})

		const [showUpdateProduct, setShowUpdateProduct] = useState(false)
		const handleShowUpdateProduct = () => setShowUpdateProduct(true); 

		const handleProductClose = () => {
			setShowUpdateProduct(false);
			setUpdateProduct ({
				name: '',
				description: '',
				price: ''
			})

			setProductData ({
				name: '',
				description: '',
				price: ''
			})
		}

		function getProductById(id) {
			fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
	        .then(res => res.json())
	        .then(data => {
	            console.log(data)
	            setUpdateProduct(data)
	            setProductData(data)
	          })

	    handleShowUpdateProduct();

		}

		function updateProductSubmit(e) {
		    
		    e.preventDefault()
		    console.log(productData._id)

		    fetch(`${process.env.REACT_APP_API_URL}/products/${productData._id}`, {
		       method: "PATCH",
		       headers: {
		           'Content-Type' : 'application/json'
		       },
		       body: JSON.stringify(updateProduct)
		    })
		    .then(res=> res.json())
		    .then(data => {
		       console.log(data)

		       if(data === true) {
		           Swal.fire({
		                   title: "Product Updated!",
		                   icon: "success",
		                   text: "Nice!"
		               }).then(()=>{window.location.reload();})

		            
		       } else {

		           Swal.fire({
		               title: "Something went wrong",
		               icon: "error",
		               text: "Please, try again."
		           }).then(()=>{window.location.reload();})
		       }
		    })
		    
		}








//-------------------------------------------------------------------------------

  
  
  const [data, setData] = useState([])
  

  useEffect(() => {
  	fetch(`${process.env.REACT_APP_API_URL}/products/showAll`)
  	.then(res => res.json())
  	.then(data => {
  		console.log(data)

  		setData(data)
			 		
  		
  	})
  }, [])





  return (
  	<>
  	<h1 className="text-center">Admin Dashboard</h1>
    <div className="d-flex justify-content-center p-3">
      <Button onClick={handleShow1} variant="danger">       
				Add Product
      </Button>
    </div>

  	<Table striped bordered hover variant="light">
  	      <thead>
  	        <tr className="table-danger">
  	          <th className="align-middle text-center">Name</th>
  	          <th className="align-middle text-center">Description</th>
  	          <th className="align-middle text-center">Price &#8369;</th>
  	          <th className="align-middle text-center">Status</th>
  	          <th className="align-middle text-center">Action</th>
  	        </tr>
  	      </thead>
  	      <tbody>
  	        {data.map(product => (
  	          <tr key={product._id}>
  	            <td className="align-middle text-center">{product.name}</td>
  	            <td className="align-middle text-center">{product.description}</td>
  	            <td className="align-middle text-center">{product.price}</td>
  	            <td className="align-middle text-center">{product.isActive ? 'Available':'Unavailable' }</td>
  	            <td className="align-middle text-center">
  	              <Button className="my-button" variant="outline-primary" onClick={()=> getProductById(product._id)}>Update</Button>
  	              <Button className="my-button mt-1" variant="outline-danger" onClick={product.isActive ? () => handleDeactivate(product._id) : () => handleActivate(product._id)}>{product.isActive ? 'Deactivate':'Activate' }</Button>
  	            </td>
  	          </tr>
  	        ))}
  	        
  	      </tbody>
  	    </Table>
  	    

  	    {/*Modal for Add Product*/}
  	    <Modal
          show={show}
          onHide={handleClose}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Add Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          	<Form onSubmit={handleSubmit}>
          	  <Form.Group className="mb-3" controlId="productName">
          	    <Form.Label>Name</Form.Label>
          	    <Form.Control 
          	    value={newProduct.name} 
          	    type="text" 
          	    placeholder="Enter product name"
          	    onChange={e => setNewProduct({ ...newProduct, name: e.target.value })}
          	    required 
          	    />
          	  </Form.Group>

          	  <Form.Group className="mb-3" controlId="productDescription">
          	    <Form.Label>Description</Form.Label>
          	    <Form.Control 
          	    as="textarea"
          	    value={newProduct.description} 
          	    type="text" 
          	    placeholder="Enter product description"
          	    onChange={e => setNewProduct({ ...newProduct, description: e.target.value })}
          	    style={{ height: '200px', width: '100%' }}
          	    required
          	    />
          	  </Form.Group>

          	  <Form.Group className="mb-3" controlId="productPrice">
          	    <Form.Label>Price</Form.Label>
          	    <Form.Control 
          	    value={newProduct.price} 
          	    type="number" 
          	    placeholder="Enter product price"
          	    onChange={e => setNewProduct({ ...newProduct, price: e.target.value })} 
          	    required
          	    />
          	  </Form.Group>

          	  <Modal.Footer>
          	    <Button variant="secondary" onClick={handleClose}>
          	      Close
          	    </Button>
          	    <Button variant="primary" type="submit">Save</Button>
          	  </Modal.Footer>
          	</Form>
          </Modal.Body>

        </Modal>

				{/*Modal for Update Product*/}
  	    <Modal
          show={showUpdateProduct}
          onHide={handleProductClose}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Update Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          	<Form onSubmit={updateProductSubmit}>        
          	  <Form.Group className="mb-3" controlId="name">
          	    <Form.Label>Product Name</Form.Label>
          	    <Form.Control 
          	        type="text"
          	        defaultValue={productData.name}      	        
          	        onChange={e => setUpdateProduct({...updateProduct, name: e.target.value})}
          	        required />
          	  </Form.Group>
          	 
          	  <Form.Group className="mb-3" controlId="description">
          	    <Form.Label>Product Description</Form.Label>
          	    <Form.Control 
          	        as="textarea" 
          	        rows={3} 
          	        defaultValue={productData.description}
          	        onChange={e => setUpdateProduct({...updateProduct, description: e.target.value})}
          	        required/>
          	  </Form.Group>

          	  <Form.Group className="mb-3" controlId="price">
          	    <Form.Label>Product Price</Form.Label>
          	    <Form.Control 
          	        type="number" 
          	        defaultValue={productData.price}
          	        onChange={e => setUpdateProduct({...updateProduct, price: e.target.value})}
          	        required/>
          	  </Form.Group>
          	  <Modal.Footer>
          	    <Button variant="secondary" onClick={handleProductClose}>
          	      Close
          	    </Button>
          	    <Button variant="primary" type="submit">Save</Button>
          	  </Modal.Footer>          	  
          	 
          	</Form>
          </Modal.Body>

        </Modal>



    </>
  );
}