import { useState, useEffect,useContext } from 'react'; 

import {Navigate} from 'react-router-dom'; 
import {useNavigate} from 'react-router-dom'; 

import Swal from 'sweetalert2'; 

import UserContext from '../UserContext';

import { Form, Button, InputGroup, Container, Tooltip, OverlayTrigger } from 'react-bootstrap';

export default function Register() {

    const {user} = useContext(UserContext)

    const navigate = useNavigate()

    // const [firstName, setFirstName] = useState("")
    // const [lastName, setLastName] = useState("")
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [isActive, setIsActive] = useState(false);


    function registerUser(e) {
        
        e.preventDefault()

        
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if (data === true) {

                Swal.fire({
                    title: "Duplicate Email Found",
                    icon: "error",
                    text: "Kindly provide another email to complete registration."
                })
            } else {

                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        // firstName: firstName,
                        // lastName: lastName,
                        email: email,
                        
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    if(data === true) {
                    
                    // setFirstName("");
                    // setLastName("")
                    setEmail("");
                    
                    setPassword1("");
                    setPassword2("");

                    Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: "Welcome!"
                        })

                        navigate("/login");

                    } else {

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please, try again."
                        })
                    }
                })
            }
        })

        
    }

    
    useEffect(() => {
           
            if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
                setIsActive(true);
            } else {
                setIsActive(false);
            }

        }, [email, password1, password2])

// ----------------------------
    // tooltip

    const emailTooltip = (props) => (
        <Tooltip id="button-tooltip" {...props}>
          Please enter your registered email address
        </Tooltip>
      );

    const passwordTooltip = (props) => (
        <Tooltip id="button-tooltip" {...props}>
          Please enter your password
        </Tooltip>
      );

    const passwordVerificationTooltip = (props) => (
        <Tooltip id="button-tooltip" {...props}>
          Please enter your password again for verification
        </Tooltip>
      );

    return (
        (user.id !== null) ? 
        <Navigate to ="/" /> 
        :
        <Container className="text-center col-7 bg-light rounded-2">
        <h1>Register</h1>
        <Form className="form rounded-2 bg-light" onSubmit={(e) => registerUser(e)}>

          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <InputGroup>
            <InputGroup.Text id="inputGroup-sizing-sm"><i className="bi bi-person"></i></InputGroup.Text>
            <Form.Control 
                type="email"
                value={email}
                onChange={(e) => {setEmail(e.target.value)}}
                placeholder="Enter email" />
            <InputGroup.Text id="inputGroup-sizing-sm">
            <OverlayTrigger
                  placement="right"
                  delay={{ show: 250, hide: 400 }}
                  overlay={emailTooltip}
                >
            <i className="bi bi-question"></i>
            </OverlayTrigger>
            
            </InputGroup.Text>
            </InputGroup>
          </Form.Group>
         
          <Form.Group className="mb-3" controlId="password1">
            <Form.Label>Password</Form.Label>
            <InputGroup>
            <InputGroup.Text id="inputGroup-sizing-sm"><i className="bi bi-key"></i></InputGroup.Text>
            <Form.Control 
                type="password" 
                value={password1}
                onChange={(e) => {setPassword1(e.target.value)}}
                placeholder="Enter Your Password" />
            <InputGroup.Text id="inputGroup-sizing-sm">
            <OverlayTrigger
                  placement="right"
                  delay={{ show: 250, hide: 400 }}
                  overlay={passwordTooltip}
                >
            <i className="bi bi-question"></i>
            </OverlayTrigger>
            
            </InputGroup.Text>
            </InputGroup>
          </Form.Group>

          <Form.Group className="mb-3" controlId="password2">
            <Form.Label>Verify Password</Form.Label>
            <InputGroup>
            <InputGroup.Text id="inputGroup-sizing-sm"><i className="bi bi-key"></i></InputGroup.Text>
            <Form.Control 
                type="password" 
                value={password2}
                onChange={(e) => {setPassword2(e.target.value)}}
                placeholder="Verify Your Password" />
            <InputGroup.Text id="inputGroup-sizing-sm">
            <OverlayTrigger
                  placement="right"
                  delay={{ show: 250, hide: 400 }}
                  overlay={passwordVerificationTooltip}
                >
            <i className="bi bi-question"></i>
            </OverlayTrigger>
            
            </InputGroup.Text>
            </InputGroup>
          </Form.Group>
          { isActive ?
                    <Button variant="danger" type="submit" id="submitBtn">
                     Submit
                    </Button>
                    :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                      Submit
                    </Button>
          }
         
        </Form> 
        </Container>
    )

}
