import { Accordion } from 'react-bootstrap'
import { useEffect, useState } from 'react'

export default function OrderHistory() {


	const [orders, setOrders] = useState([])

	useEffect(() => {
	  fetch(`${process.env.REACT_APP_API_URL}/orders/all`)
	    .then(res => res.json())
	    .then(data => {
	      console.log(data)

	      setOrders(data)



	    })
	}, [])

	useEffect(() => {
	  console.log(orders)
	}, [orders])


	 


	return (
	  <>
	    <h1 className="text-center">Order History</h1>
	    <Accordion defaultActiveKey="0">
	      {orders.map((order, index) => (
	        <Accordion.Item eventKey={index} key={index}>
	          <Accordion.Header>Orders for userId: {order._id.email}</Accordion.Header>
	          
	            {order.orders.map((item, index) => (
	              <Accordion.Body key={index}>
	                <strong className="my-0">Purchased On - {new Date(item.purchasedOn).toLocaleDateString()}</strong>	      
	                  {item.products.map((product, index) => (
	                    <ul className="my-1" key={index}>
	                      <li>Product - {product.name}</li>
	                      <li>Quantity - {product.quantity}</li>
	                    </ul>
	                  ))}
	                <strong className="text-danger">Total Amount - &#8369;{item.totalAmount.toLocaleString()}</strong>
	                                 
	              </Accordion.Body>
	            ))}
	          
	        </Accordion.Item>
	      ))}
	    </Accordion>
	  </>
	);



};