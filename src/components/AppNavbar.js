import './myStyles.css'

import { useContext, useState } from 'react';
import{ NavLink } from 'react-router-dom';

import {Nav, Navbar, Container, Offcanvas } from 'react-bootstrap';

import UserContext from '../UserContext';

export default function AppNavbar() {


  const { user } = useContext(UserContext);

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
 
         <>
            <Navbar bg="light" expand="lg" className="mb-3">
              <Container fluid>
                <span className="bi bi-list mt-1 me-3" onClick={handleShow} style={{fontSize: '30px'}}> </span>
                <Navbar.Brand href={!user.isAdmin ? "" : "/dashboard"} className="me-auto">Dashboard</Navbar.Brand>               
                <Navbar.Offcanvas
                  show={show}
                  onHide={handleClose}
                  id="offcanvasNavbar"
                  aria-labelledby="offcanvasNavbarLabel"
                  placement="start"
                >
                  <Offcanvas.Header closeButton>
                    <Offcanvas.Title id="offcanvasNavbarLabel">
                      Dashboard
                    </Offcanvas.Title>
                  </Offcanvas.Header>
                  <Offcanvas.Body>
                    <Nav className="justify-content-end flex-grow-1 pe-3">
                      <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                      <Nav.Link as={NavLink} to="/products" className={!user.isAdmin ? '' : 'd-none'}>Products</Nav.Link>
                      <Nav.Link as={NavLink} to="/orderhistory" className={!user.isAdmin ? 'd-none' :''  }>Order History</Nav.Link>
                                            
                      {(user.id !== null) ? 
                        <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                        :
                        <>      
                        <Nav.Link as={NavLink} to="/login">Login</Nav.Link> 
                        <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                        </>
                      }
                    </Nav>
                  </Offcanvas.Body>
                </Navbar.Offcanvas>
              </Container>
            </Navbar>


        </>
        
  );
}

