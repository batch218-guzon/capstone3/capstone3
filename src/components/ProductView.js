import { useState, useEffect, useContext } from 'react';
import { Container, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link  } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate()
	
	const { productId } = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	
	
	const [quantity, setQuantity] = useState(0);

	useEffect(() => {

		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			
			
			console.log(data.isActive)


		

		})


	}, [productId])


	function Checkout() {

		fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
			method: 'POST',
			headers: {
				
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity,
				userId: user.id,
				isAdmin: user.isAdmin
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === true) {

			    Swal.fire({
			        title: "Order created!",
			        icon: "success",
			        text: "Your order has been successfully created.",
			        showClass: {
			            popup: 'animate__animated animate__fadeInDown'
			          },
			          hideClass: {
			            popup: 'animate__animated animate__fadeOutUp'
			          }
			    }).then(navigate("/products"))
			} else {
				Swal.fire({
			        title: "Error",
			        icon: "error",
			        text: "Order encountered some problems.",
			        showClass: {
			            popup: 'animate__animated animate__fadeInDown'
			          },
			          hideClass: {
			            popup: 'animate__animated animate__fadeOutUp'
			          }
			    })
			}
		})
	}

	return (
		<>
		<Container className="card text-center col-5 mt-5 p-5">
			<div>
				<h1>{name}</h1>
				<p>{description}</p>
				<p>&#8369; {price}</p>
			</div>
			{
			(user.id !== null) ?
			<>
				<div>
					<div className="d-flex justify-content-center my-3">
				      <Button variant="danger" onClick={() => setQuantity(quantity - 1) } disabled={quantity === 0} >-</Button>
				        <span className="mx-4 px-">{quantity}</span>
				      <Button variant="danger" onClick={() => setQuantity(quantity + 1)}>+</Button>
				    </div>

				    <Button className="my-button-again" variant="primary" onClick={()=> Checkout()} disabled={quantity === 0}>Checkout</Button> 
				    <Button variant="danger" as={Link} to="/products">Back</Button>
			    </div>
			</>
			    :

			    <Button className="btn btn-primary" as={Link} to="/login" >Log in to Buy</Button>

			
			}
			  
		    										
		</Container>
		</>

		

		
	)

}