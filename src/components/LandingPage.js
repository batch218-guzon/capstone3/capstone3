import { Row, Col } from 'react-bootstrap';



export default function LandingPage({data}) {

    const {title, content} = data;

return (
    <Row>
    	<Col className="p-5">
            <h1>{title}</h1>
            <p>{content}</p>

        </Col>
    </Row>
	)
}