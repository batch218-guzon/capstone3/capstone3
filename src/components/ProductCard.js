

import {Card, Button, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function ProductCard({product}) {
	
	const {name, description, price, _id} = product;


  return (

    <Col>
      <Card className="my-card text-center">
        <Card.Img variant="top" src="holder.js/100px160" />
        <Card.Body className="my-card-body">
          <Card.Title>{name}</Card.Title>
          <Card.Text className="my-card-text">
            {description}
          </Card.Text>
        </Card.Body>
        <Button className="bg-primary m-4" as={Link} to={`/products/${_id}`} >Details</Button>
        <Card.Footer>
          <small className="text-muted">&#8369; {price}</small>
        </Card.Footer>  
      </Card>
    </Col>
  )
}

ProductCard.propTypes = {
  // "shape" method is used to check if prop object conforms to a specific "shape"
  product: PropTypes.shape({
    // Defined properties and their expected types
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    
  })
}